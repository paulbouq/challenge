import requests, json, time

def find_min(values):
    # we can use builtin method min ?
    mini = values(0)
    for val in values:
        if(val < mini):
            mini = val
    return mini

def find_max(values):
    # we can use builtin method max ?
    maxi = values(0)
    for val in values:
        if(val > maxi):
            maxi = val
    return maxi

def compute_avr(values):
    # "values" might be a long list
    # so using len(values) would use time
    # we can get the sum of the values and its length in one loop
    length = 0
    avr = 0.0
    for val in values:
        length += 1
        avr += val
    return avr / length

# Client Credentials Authentication
# Inspired by https://dev.netatmo.com/resources/technical/samplessdks/codesamples#clientcredentials
payload = {"client_id":     "john_doe_id",
           "client_secret": "john_doe_secret",
           "grant_type":    "password",
           "username":      "john.doe@gmail.com",
           "password":      "123456",
           "scope":         "read_station"
           }
try:
    response = requests.post("https://api.netatmo.com/oauth2/token", data=payload)
    response.raise_for_status()
    access_token = response.json()["access_token"]

    nbr_days = 7

    # Execution time
    now = time.time()

    # Delay, in seconds, of the corresponding number of days
    delay = nbr_days * 24 * 60 * 60

    # Request parameters
    params = {
    "access_token": access_token,
    "device_id":    "42:42:42:42:42:42",

    # As requested for the challenge
    "scale": "max",

    # We are only interested in the temperature
    "type": "Temperature",

    # Starting "nbr_days" ago
    "date_begin":   str(now - delay),
    "date_end":     str(now)

    }

    # Retrieving data
    try:
        URL = "https://api.netatmo.com/api/getmeasure"
        response = requests.post(URL, params=params)
        response.raise_for_status()
        data = response.json()["body"]

        # Computing interesting values
        minimum = find_min(data["value"])
        maximum = find_max(data["value"])
        average = compute_avr(data["value"])

        # Printing messages
        print("Temperature for the last {} days".format(nbr_days))
        print("MIN      : {}".format(minimum))
        print("MAX      : {}".format(maximum))
        print("AVERAGE  : {}".format(average))

    except requests.exceptions.HTTPError as error:
        print(error.response.status_code, error.response.text)

except requests.exceptions.HTTPError as error:
    print(error.response.status_code, error.response.text)
